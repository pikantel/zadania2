import funkcje


if __name__ == '__main__':
    while True:
        a = funkcje.getInput('Liczba a')
        b = funkcje.getInput('Liczba b')
        prompt = int(input('Wybierz działanie (1: dodawanie, 2: odejmowanie, 3: mnożenie, 4: dzielenie, 5: modulo): '))

        if prompt == 1:
            print(funkcje.dodaj(a, b))
        elif prompt == 2:
            print(funkcje.odejmij(a, b))
        elif prompt == 3:
            print(funkcje.pomnoz(a, b))
        elif prompt == 4:
            print(funkcje.podziel(a, b))
        elif prompt == 5:
            print(int((funkcje.modulo(a, b))))
        else:
            print("ERROR")

        flow = input('Press q to exit, any other key to continue ')
        if flow == 'q':
            print('Thank you for using MikolajC software')
            break